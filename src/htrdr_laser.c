/* copie de htrdr_sun pour adaptation à laser */
/* Copyright (C) 2018, 2019, 2020 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2018, 2019 CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "htrdr.h"
#include "htrdr_c.h"
#include "htrdr_laser.h"

#include <rsys/algorithm.h>
#include <rsys/dynamic_array_double.h>
#include <rsys/ref_count.h>
#include <rsys/math.h>
#include <rsys/cstr.h>

#include <rsys/double33.h>
#include <rsys/double2.h>
#include <rsys/double3.h>
#include <rsys/float2.h>
#include <rsys/float3.h>

#include <aw.h>
#include <star/ssp.h>
#include <star/s3d.h>
#include <star/s3daw.h>

struct ray_context {
  float range[2];
  struct s3d_hit hit_prev;
  int id[2];
};
#define RAY_CONTEXT_NULL__ {{0,INF}, S3D_HIT_NULL__, {0,0}}
static const struct ray_context RAY_CONTEXT_NULL = RAY_CONTEXT_NULL__;

struct trace_laser_context {
  struct s3d_scene_view* view;
  struct ray_context context;
  struct s3d_hit* hit;
};
static const struct trace_laser_context TRACE_LASER_CONTEXT_NULL = {
  NULL, RAY_CONTEXT_NULL__, NULL
};

struct htrdr_laser {
  /* Short wave radiance in W.m^-2.sr^-1, for each spectral interval */
  struct darray_double radiances_sw;

  /* Short wave spectral interval boundaries, in cm^-1 */
  struct darray_double wavenumbers_sw;

  struct s3d_scene_view* view;
  double origin[3] ; /* coordonnées du point */

  ref_T ref;
  struct htrdr* htrdr;
};

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
release_laser(ref_T* ref)
{
  struct htrdr_laser* laser;
  ASSERT(ref);
  laser = CONTAINER_OF(ref, struct htrdr_laser, ref);
  darray_double_release(&laser->radiances_sw);
  darray_double_release(&laser->wavenumbers_sw);
  if(laser->view) S3D(scene_view_ref_put(laser->view));
  MEM_RM(laser->htrdr->allocator, laser);
}

/* Check that `hit' roughly lies on an edge. For triangular primitives, a
 * simple but approximative way is to test that its position have at least one
 * barycentric coordinate roughly equal to 0 or 1. */
static FINLINE int
hit_on_edge(const struct s3d_hit* hit)
{
  const float on_edge_eps = 1.e-4f;
  float w;
  ASSERT(hit && !S3D_HIT_NONE(hit));
  w = 1.f - hit->uv[0] - hit->uv[1];
  return eq_epsf(hit->uv[0], 0.f, on_edge_eps)
      || eq_epsf(hit->uv[0], 1.f, on_edge_eps)
      || eq_epsf(hit->uv[1], 0.f, on_edge_eps)
      || eq_epsf(hit->uv[1], 1.f, on_edge_eps)
      || eq_epsf(w, 0.f, on_edge_eps)
      || eq_epsf(w, 1.f, on_edge_eps);
}

static int
laser_filter
  (const struct s3d_hit* hit,
   const float ray_org[3],
   const float ray_dir[3],
   void* ray_data,
   void* filter_data)
{
  const struct ray_context* ray_ctx = ray_data;
  (void)ray_org, (void)ray_dir, (void)filter_data;

  if(!ray_ctx) return 0;

  if(S3D_PRIMITIVE_EQ(&hit->prim, &ray_ctx->hit_prev.prim)) return 1;

  if(!S3D_HIT_NONE(&ray_ctx->hit_prev) && eq_epsf(hit->distance, 0, 1.e-1f)) {
    /* If the targeted point is near of the origin, check that it lies on an
     * edge/vertex shared by the 2 primitives. */
    return hit_on_edge(&ray_ctx->hit_prev) && hit_on_edge(hit);
  }

  return hit->distance <= ray_ctx->range[0]
      || hit->distance >= ray_ctx->range[1];
}

static res_T
setup_laser(struct htrdr_laser* laser, const char* obj_filename)
{
  struct s3d_scene* scn = NULL;
  struct s3daw* s3daw = NULL;
  /*
  struct aw_obj* obj = NULL;
  struct aw_obj_named_group* mtl = NULL,
  struct aw_obj_face face;
  struct aw_obj_vertex vertex;
  struct aw_obj_vertex_data vdata;
  */
  const unsigned iprim=0;
  struct s3d_primitive prim ;
  struct s3d_attrib attrib ; 

  size_t nshapes;
  size_t ishape;
  float lower[3] ;
  float upper[3];
  res_T res = RES_OK;
  ASSERT(laser);

  if(!obj_filename) {
    /* No geometry! Discard the creation of the scene view */
    htrdr_log_warn(laser->htrdr,
      "%s: the scene does not have laser geometry.\n",
      FUNC_NAME);
    goto exit;
  }

  res = s3daw_create(&laser->htrdr->logger, laser->htrdr->allocator, NULL,
    NULL, laser->htrdr->s3d, laser->htrdr->verbose, &s3daw);
  if(res != RES_OK) {
    htrdr_log_err(laser->htrdr,
      "%s: could not create the Star-3DAW device -- %s.\n",
      FUNC_NAME, res_to_cstr(res));
    goto error;
  }

  res = s3daw_load(s3daw, obj_filename);
  if(res != RES_OK) {
    htrdr_log_err(laser->htrdr,
      "%s: could not load the obj file `%s' -- %s.\n",
      FUNC_NAME, obj_filename, res_to_cstr(res));
    goto error;
  }

  /* On prend le premier sommet de la géom de la source 
  AW(obj_get_face(obj, mtl->face_id, &face));
  AW(obj_get_vertex(obj, face.vertex_id, &vertex));
  AW(obj_get_vertex_data(obj, &vertex, &vdata));*/

  res = s3d_scene_create(laser->htrdr->s3d, &scn);
  if(res != RES_OK) {
    htrdr_log_err(laser->htrdr,
      "%s: could not create the Star-3D scene of the laser -- %s.\n",
      FUNC_NAME, res_to_cstr(res));
    goto error;
  }

  S3DAW(get_shapes_count(s3daw, &nshapes));
  FOR_EACH(ishape, 0, nshapes) {
    struct s3d_shape* shape;
    S3DAW(get_shape(s3daw, ishape, &shape));
    res = s3d_mesh_set_hit_filter_function(shape, laser_filter, NULL);
    if(res != RES_OK) {
      htrdr_log_err(laser->htrdr,
        "%s: could not setup the hit filter function of the laser geometry "
        "-- %s.\n", FUNC_NAME, res_to_cstr(res));
      goto error;
    }
    res = s3d_scene_attach_shape(scn, shape);
    if(res != RES_OK) {
      htrdr_log_err(laser->htrdr,
        "%s: could not attach the laser geometry to its Star-3D scene -- %s.\n",
        FUNC_NAME, res_to_cstr(res));
      goto error;
    }
  }
  res = s3d_scene_view_create(scn,S3D_GET_PRIMITIVE|S3D_TRACE, &laser->view);
  if(res != RES_OK) {
    htrdr_log_err(laser->htrdr,
      "%s: could not create the Star-3D scene view of the laser geometry "
      "-- %s.\n", FUNC_NAME, res_to_cstr(res));
    goto error;
  }

  S3D(scene_view_get_primitive(laser->view, iprim, &prim));
  if(res != RES_OK) {
    htrdr_log_err(laser->htrdr,
      "%s: could not get the laser primitive -- %s.\n",
      FUNC_NAME, res_to_cstr(res));
    goto error;
  }
  res = s3d_triangle_get_vertex_attrib(&prim, 0, S3D_POSITION,&attrib);
  if(res != RES_OK) {
    htrdr_log_err(laser->htrdr,
      "%s: could not get the laser 1st vertex -- %s.\n",
      FUNC_NAME, res_to_cstr(res));
    goto error;
  }
  laser->origin[0]=attrib.value[0];
  laser->origin[1]=attrib.value[1];
  laser->origin[2]=attrib.value[2];

  res = s3d_scene_view_get_aabb(laser->view, lower, upper);
  if(res != RES_OK) {
    htrdr_log_err(laser->htrdr,
      "%s: could not get the laser bounding box -- %s.\n",
      FUNC_NAME, res_to_cstr(res));
    goto error;
  }

exit:
  if(s3daw) S3DAW(ref_put(s3daw));
  if(scn) S3D(scene_ref_put(scn));
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
htrdr_laser_create(struct htrdr* htrdr,
	           const char * obj_filename, 
	   	   struct htrdr_laser** out_laser)
{
  const double incoming_flux_sw[] = { /* In W.m^-2 */
    0.00, 1368.1, 0.0
  };
  const double wavenumbers_sw[] = { /* In cm^-1 */
    820.000, 18850, 18870, 49999.0
  };

  const size_t nspectral_intervals = sizeof(incoming_flux_sw)/sizeof(double);
  struct htrdr_laser* laser = NULL;
  size_t i;
  res_T res = RES_OK;
  ASSERT(htrdr && out_laser);
  ASSERT(sizeof(wavenumbers_sw)/sizeof(double) == nspectral_intervals+1);

  laser = MEM_CALLOC(htrdr->allocator, 1, sizeof(*laser));
  if(!laser) {
    htrdr_log_err(htrdr, "could not allocate the laser data structure.\n");
    res = RES_MEM_ERR;
  goto error;
}
  ref_init(&laser->ref);
  laser->htrdr = htrdr;
  darray_double_init(htrdr->allocator, &laser->radiances_sw);
  darray_double_init(htrdr->allocator, &laser->wavenumbers_sw);

  res = darray_double_resize(&laser->radiances_sw, nspectral_intervals);
  if(res != RES_OK) {
    htrdr_log_err(htrdr,
      "could not allocate the list of per spectral band radiance of the laser.\n");
    goto error;
  }
  res = darray_double_resize(&laser->wavenumbers_sw, nspectral_intervals+1);
  if(res != RES_OK) {
    htrdr_log_err(htrdr,
      "could not allocate the list of spectral band boundaries of the laser.\n");
    goto error;
  }

  FOR_EACH(i, 0, darray_double_size_get(&laser->radiances_sw)) {
    /* Convert the incoming flux in radiance */
    darray_double_data_get(&laser->radiances_sw)[i] = incoming_flux_sw[i] / PI;
  }
  FOR_EACH(i, 0, darray_double_size_get(&laser->wavenumbers_sw)) {
    darray_double_data_get(&laser->wavenumbers_sw)[i] = wavenumbers_sw[i];
  }


  res = setup_laser(laser, obj_filename);
  if(res != RES_OK) goto error;

exit:
  *out_laser = laser;
  return res;
error:
  if(laser) {
    htrdr_laser_ref_put(laser);
    laser = NULL;
  }
  goto exit;
}

void
htrdr_laser_ref_get(struct htrdr_laser* laser)
{
  ASSERT(laser);
  ref_get(&laser->ref);
}

void
htrdr_laser_ref_put(struct htrdr_laser* laser)
{
  ASSERT(laser);
  ref_put(&laser->ref, release_laser);
}

double
htrdr_laser_get_radiance(const struct htrdr_laser* laser, const double wavelength)
{
  const double* wavenumbers;
  const double wnum = wavelength_to_wavenumber(wavelength);
  const double* wnum_upp;
  size_t nwavenumbers;
  size_t ispectral_band;
  ASSERT(laser && wavelength > 0);

  wavenumbers = darray_double_cdata_get(&laser->wavenumbers_sw);
  nwavenumbers = darray_double_size_get(&laser->wavenumbers_sw);
  ASSERT(nwavenumbers);

  if(wnum < wavenumbers[0] || wnum > wavenumbers[nwavenumbers-1]) {
    htrdr_log_warn(laser->htrdr,
      "the submitted wavelength is outside the laser spectrum.\n");
  }

  wnum_upp = search_lower_bound
    (&wnum, wavenumbers, nwavenumbers, sizeof(double), cmp_dbl);

  if(!wnum_upp) { /* Clamp to the upper spectral band */
    ispectral_band = nwavenumbers - 2;
    ASSERT(ispectral_band == darray_double_size_get(&laser->radiances_sw)-1);
  } else if(wnum_upp == wavenumbers) { /* Clamp to the lower spectral band */
    ispectral_band = 0;
  } else {
    ispectral_band = (size_t)(wnum_upp - wavenumbers - 1);
  }
  return darray_double_cdata_get(&laser->radiances_sw)[ispectral_band];
}

size_t
htrdr_laser_get_spectral_bands_count(const struct htrdr_laser* laser)
{
  ASSERT(laser);
  return darray_double_size_get(&laser->radiances_sw);
}

void
htrdr_laser_get_spectral_band_bounds
  (const struct htrdr_laser* laser,
   const size_t ispectral_band,
   double bounds[2])
{
  size_t iband_wlen;
  const double* wnums;
  ASSERT(laser && ispectral_band < htrdr_laser_get_spectral_bands_count(laser));
  ASSERT(ispectral_band + 1 < darray_double_size_get(&laser->wavenumbers_sw));

  /* The spectral bands are internally defined with wavenumbers, while the laser
   * API uses wavelengths. But since wavelengths are inversely proportionnal to
   * wavenumbers, one have to reversed the spectral band index */
  iband_wlen = htrdr_laser_get_spectral_bands_count(laser) - 1 - ispectral_band;
  wnums = darray_double_cdata_get(&laser->wavenumbers_sw);
  bounds[1] = wavenumber_to_wavelength(wnums[iband_wlen+0]);
  bounds[0] = wavenumber_to_wavelength(wnums[iband_wlen+1]);
  ASSERT(bounds[0] <= bounds[1]);
}

void
htrdr_laser_get_CIE_XYZ_spectral_bands_range
  (const struct htrdr_laser* laser, size_t band_range[2])
{
  /* Bounds of the X, Y and Z functions as defined by the CIE */
  const double nu_min = wavelength_to_wavenumber(SW_WAVELENGTH_MIN);/*In cm^-1*/
  const double nu_max = wavelength_to_wavenumber(SW_WAVELENGTH_MAX);/*In cm^-1*/
  const double* wnums = NULL;
  size_t iband_low_nu = SIZE_MAX;
  size_t iband_upp_nu = SIZE_MAX;
  size_t i;
  ASSERT(laser && band_range);

  wnums = darray_double_cdata_get(&laser->wavenumbers_sw);

  /* Perform a linear search since the number of spectral bands is small */
  FOR_EACH(i, 0, htrdr_laser_get_spectral_bands_count(laser)) {
    if(wnums[i] <= nu_min && nu_min < wnums[i+1]) iband_low_nu = i;
    if(wnums[i] <= nu_max && nu_max < wnums[i+1]) iband_upp_nu = i;
  }
  ASSERT(iband_low_nu <= iband_upp_nu);

  /* Transform the band index from "wavenumber space" to "wavelength space" */
  band_range[0] = htrdr_laser_get_spectral_bands_count(laser) - 1 - iband_upp_nu;
  band_range[1] = htrdr_laser_get_spectral_bands_count(laser) - 1 - iband_low_nu;
  ASSERT(band_range[0] <= band_range[1]);
}

res_T
htrdr_laser_trace_ray
  (struct htrdr_laser* laser,
   const double org[3],
   const double dir[3], /* Must be normalized */
   const double range[2],
   const struct s3d_hit* prev_hit, 
   struct s3d_hit* hit)
{
  struct ray_context ray_ctx = RAY_CONTEXT_NULL;
  float ray_org[3];
  float ray_dir[3];
  res_T res = RES_OK;
  ASSERT(laser && org && dir && range && hit);

  if(!laser->view) { /* No laser geometry */
    *hit = S3D_HIT_NULL;
    goto exit;
  }

  f3_set_d3(ray_org, org);
  f3_set_d3(ray_dir, dir);
  f2_set_d2(ray_ctx.range, range);
  ray_ctx.hit_prev = prev_hit ? *prev_hit : S3D_HIT_NULL;

  res = s3d_scene_view_trace_ray
    (laser->view, ray_org, ray_dir, ray_ctx.range, &ray_ctx, hit);
  if(res != RES_OK) {
    htrdr_log_err(laser->htrdr,
      "%s: could not trace the ray against the laser geometry -- %s.\n",
      FUNC_NAME, res_to_cstr(res));
    goto error;
  }
  
exit:
  return res;
error:
  goto exit;
}


res_T 
htrdr_laser_get_origin(struct htrdr_laser * laser, double orig[3])
{
  res_T res = RES_OK ; 
  ASSERT(orig);
  orig[0] = laser->origin[0];
  orig[1] = laser->origin[1];
  orig[2] = laser->origin[2];
  return res;
}

