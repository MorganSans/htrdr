/* copie de htrdr_sun.h pour adapter htrdr_laser.h */
/* Copyright (C) 2018, 2019, 2020 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2018, 2019 CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef HTRDR_LASER_H
#define HTRDR_LASER_H

#include <rsys/rsys.h>

/* Forward declaration */
struct htrdr;
struct htrdr_laser;
struct ssp_rng;
struct s3d_hit;

extern LOCAL_SYM res_T
htrdr_laser_create
  (struct htrdr* htrdr,
   const char * obj_filename,
   struct htrdr_laser** out_laser);

extern LOCAL_SYM void
htrdr_laser_ref_get
  (struct htrdr_laser* laser);

extern LOCAL_SYM void
htrdr_laser_ref_put
  (struct htrdr_laser* laser);

extern LOCAL_SYM double /* W.sr^-1.m^-2 */
htrdr_laser_get_radiance
  (const struct htrdr_laser* laser,
   const double wavelength);

extern LOCAL_SYM size_t
htrdr_laser_get_spectral_bands_count
  (const struct htrdr_laser* laser);

extern LOCAL_SYM void
htrdr_laser_get_spectral_band_bounds
  (const struct htrdr_laser* laser,
   const size_t ispectral_band,
   double bounds[2]); /* Lower and upper wavelength in nanometer */

/* Return the ranges of the spectral bands where the CIE XYZ color space is
 * defined. CIE XYZ in [band_range[0], band_range[1]] */
extern LOCAL_SYM void
htrdr_laser_get_CIE_XYZ_spectral_bands_range
  (const struct htrdr_laser* laser,
   size_t band_range[2]);

extern LOCAL_SYM res_T
htrdr_laser_trace_ray
  (struct htrdr_laser* laser,
   const double org[3],
   const double dir[3], /* Must be normalized */
   const double range[2],
   const struct s3d_hit* prev_hit,
   struct s3d_hit* hit);

extern LOCAL_SYM res_T
htrdr_laser_get_origin
  (struct htrdr_laser * laser,
   double * origin);

#endif /* HTRDR_LASER_H */
